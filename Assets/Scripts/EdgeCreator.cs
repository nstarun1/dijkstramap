﻿#if (UNITY_EDITOR) 
using UnityEngine;
using UnityEditor;
using System.Collections;

public class EdgeCreator : EditorWindow
{
	private GameObject prefab;
	private Transform parentTransform;
	private Vector3 scale = new Vector3(1, 1, 1);
	
	[MenuItem ("Window/Edge Creator")]

	public static void ShowWindow ()
	{
		GetWindow(typeof(EdgeCreator));
	}
	
	void OnGUI ()
	{
		titleContent.text = "Edge Creator";

		prefab = (GameObject) EditorGUILayout.ObjectField("Prefab", prefab, typeof(GameObject), true);
		parentTransform = (Transform) EditorGUILayout.ObjectField("Edge Group", parentTransform, typeof(Transform), true);
		scale = EditorGUILayout.Vector3Field("Initial Scale", scale);
		
		if (GUILayout.Button("Make Edge"))
		{
			CheckMakeEdge();
		}
	}

	void CheckMakeEdge()
	{
		if (Selection.objects.Length == 2)
		{
			GameObject edge = (GameObject) PrefabUtility.InstantiatePrefab(prefab);
			edge.transform.SetParent(parentTransform);
			edge.transform.localScale = scale;
				
			GameObject vertexA = (GameObject) Selection.objects[0];
			GameObject vertexB = (GameObject) Selection.objects[1];

			MakeEdge(vertexA, vertexB, edge);
			
			Selection.objects = new Object[0];
		}
	}

	void MakeEdge(GameObject vertexA, GameObject vertexB, GameObject edge)
	{
		Vector3 vertexAPosition = vertexA.transform.position;
		Vector3 vertexBPosition = vertexB.transform.position;
		Vector3 vertexPositionDifference = vertexBPosition - vertexAPosition;

		edge.transform.position = Vector3.Lerp (vertexAPosition, vertexBPosition, 0.5f);
		
		edge.transform.localScale = new Vector3(edge.transform.localScale.x, vertexPositionDifference.magnitude * 0.5f, edge.transform.localScale.z);
		edge.transform.rotation = Quaternion.LookRotation(vertexPositionDifference);
		edge.transform.Rotate(new Vector3 (90, 0, 0));
	}
}
#endif