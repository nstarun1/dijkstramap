﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RandomMesh : MonoBehaviour
{
	[SerializeField] private DijkstraController _dijkstraController;
	
	[SerializeField] private GameObject _vertexPrefab;
	[SerializeField] private GameObject _edgePrefab;
	
	[SerializeField] private Transform _vertexContainer;
	[SerializeField] private Transform _edgeContainer;

	[SerializeField] public Vector2 _xRange;
	[SerializeField] public Vector2 _yRange;
	[SerializeField] public Vector2 _zRange;

	[SerializeField] public Vector2 _vertexCountRange;
	[SerializeField] public Vector2 _edgeCountRange;

	private List<GameObject> _vertexList;
	private List<GameObject> _edgeList;

	public void Awake()
	{
		_vertexList = new List<GameObject>();
		_edgeList = new List<GameObject>();

		int vertexCount = Mathf.FloorToInt(Random.Range(_vertexCountRange.x, _vertexCountRange.y));
		int edgeCount = Mathf.FloorToInt(Random.Range(_edgeCountRange.x, _edgeCountRange.y));

		for (int i = 0; i < vertexCount; i++)
		{
			_vertexList.Add(RandomVertex());
		}

		for (int i = 0; i < edgeCount; i++)
		{
			GameObject vertex1 = _vertexList[Random.Range(0, _vertexList.Count)];
			GameObject vertex2 = vertex1;

			while (vertex1 == vertex2)
			{
				vertex2 = _vertexList[Random.Range(0, _vertexList.Count)];
			}

			_edgeList.Add(ConnectVertex(vertex1, vertex2));
		}

		_dijkstraController.StartNode = _vertexList.First().GetComponent<VertexScript>();
		_dijkstraController.GoalNode = _vertexList.Last().GetComponent<VertexScript>();
	}

	private GameObject RandomVertex()
	{
		GameObject vertex = Instantiate(_vertexPrefab, _vertexContainer);

		vertex.transform.position = RandomVector3();

		return vertex;
	}

	private Vector3 RandomVector3()
	{
		return new Vector3(
			Random.Range(_xRange.x, _xRange.y),
			Random.Range(_yRange.x, _yRange.y),
			Random.Range(_zRange.x, _zRange.y)
		);
	}

	private GameObject ConnectVertex(GameObject vertexA, GameObject vertexB)
	{
		Vector3 vertexAPosition = vertexA.transform.position;
		Vector3 vertexBPosition = vertexB.transform.position;
		Vector3 vertexPositionDifference = vertexBPosition - vertexAPosition;

		GameObject edge = Instantiate(_edgePrefab, _edgeContainer);
		edge.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
		edge.transform.position = Vector3.Lerp (vertexAPosition, vertexBPosition, 0.5f);
		
		edge.transform.localScale = new Vector3(edge.transform.localScale.x, vertexPositionDifference.magnitude * 0.5f, edge.transform.localScale.z);
		edge.transform.rotation = Quaternion.LookRotation(vertexPositionDifference);
		edge.transform.Rotate(new Vector3 (90, 0, 0));
		
		return edge;
	}
}
