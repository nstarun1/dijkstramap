﻿using UnityEngine;
using System.Collections;

public class GraphVisuals : MonoBehaviour
{
	public Material[] VertexMaterialsByHeight
						= new Material[5];

	public Material EdgeUnvisitedMaterial;
	public Material EdgeVisitedMaterial;
	public Material EdgeInQueueMaterial;
	public Material ShortestPathMaterial;

	public float EdgeThickness = 0.5f;

	public Material GetVertexMaterialByHeight(float y)
	{
		float heightAsPercentage = (y - GraphConstants.MIN_Y)
			  / (GraphConstants.MAX_Y - GraphConstants.MIN_Y);
		float scaledByMaterialsCount = heightAsPercentage * VertexMaterialsByHeight.Length;
		int index = Mathf.Clamp(
			(int)scaledByMaterialsCount,
			0,
			VertexMaterialsByHeight.Length - 1);
		return VertexMaterialsByHeight[index];
	}

	public void StretchEdgeBetweenTwoVertices(VertexScript vertexA, VertexScript vertexB, EdgeScript edge)
	{
		Vector3 leftVertexPosition = vertexA.transform.position;
		Vector3 rightVertexPosition = vertexB.transform.position;
		edge.transform.position = Vector3.Lerp (leftVertexPosition, rightVertexPosition, 0.5f);
		Vector3 positionDifferences = rightVertexPosition - leftVertexPosition;
		edge.transform.localScale = new Vector3(edge.transform.localScale.x, positionDifferences.magnitude * 0.5f, edge.transform.localScale.z);
		edge.transform.rotation = Quaternion.LookRotation (positionDifferences);
		edge.transform.Rotate (new Vector3 (90, 0, 0));
	}
}
