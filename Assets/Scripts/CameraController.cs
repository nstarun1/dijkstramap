﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public float PanSpeed = 1f;

	void Update ()
	{
		float deltaX = Input.GetAxis ("Horizontal");
		float deltaY = Input.GetAxis ("Vertical");
		if (deltaX != 0f || deltaY != 0f)
		{
			transform.Translate (deltaX * PanSpeed, deltaY * PanSpeed, 0f);
		}

		float deltaZoom = Input.GetAxis ("Zoom");
		if (deltaZoom != 0f)
		{
			Camera.main.orthographicSize += deltaZoom;
		}
	}
}
