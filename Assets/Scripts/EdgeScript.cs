﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class EdgeScript : MonoBehaviour
{
	public VertexScript VertexA;
	public VertexScript VertexB;
	public float Cost;

	public bool Stretch = true;

	private GraphVisuals graphVisuals;

	void Start()
	{
		VertexA = VertexB = null;

		GameObject singletons = GameObject.Find ("Singletons");
		graphVisuals = singletons.GetComponent<GraphVisuals> ();
	}

	void Update ()
	{
		if (Stretch && VertexA && VertexB)
		{
			graphVisuals.StretchEdgeBetweenTwoVertices (VertexA, VertexB, this);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		VertexScript vertex = other.gameObject.GetComponent<VertexScript> ();
		if (vertex)
		{
			if (VertexA == null && VertexB != vertex)
			{
				VertexA = vertex;
			}
			else if (VertexB == null && VertexA != vertex)
			{
				VertexB = vertex;
			}
			else if (VertexA && VertexB)
			{
				System.Console.WriteLine ("ERROR: Edge with two vertices intersecting new vertex in OnTriggerEnter.");
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		VertexScript vertex = other.gameObject.GetComponent<VertexScript> ();
		if (vertex)
		{
			if (VertexA == vertex)
			{
				VertexA = null;
			}
			else if (VertexB == vertex)
			{
				VertexB = null;
			}
		}
	}

	public VertexScript GetOtherVertex(VertexScript vertex)
	{
		VertexScript value = null;
		if (vertex == null)
		{
			System.Console.WriteLine ("ERROR: null vertex in GetOtherVertex.");
		}
		else if (vertex == VertexA)
		{
			value = VertexB;
		}
		else if (vertex == VertexB)
		{
			value = VertexA;
		}
		else
		{
			System.Console.WriteLine ("ERROR: vertex not connected to edge in GetOtherVertex.");
		}
		return value;
	}
}
