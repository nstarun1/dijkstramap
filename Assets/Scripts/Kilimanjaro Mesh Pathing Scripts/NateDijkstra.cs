﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class NateDijkstra : MonoBehaviour {
    public float DelayBetweenSteps = 1f;
    // lets me slow down or speed up the process

    public GameObject Visited;
    public GameObject Path;
    [Range (0f, 10f)]
    public float VerticalTraversalCostMultiplier = 1f;

    private NateVertex StartNode;
    // where Dijkstra's algorithm should start
    private NateVertex GoalNode;
    // the other end of the shortest path we are looking for
    private bool initialized = false;
    // flag for whether the algorithm has been initialized; notice that it is in the past tense
    private bool shortestPathMarked = false;
    // flag for whether the shortest path has been marked; notice that it is in the past tense
    private float timeOfNextStep = float.PositiveInfinity;
    // when the next step of the algorithm should occur; see PerformAlgorithm

    private IPriorityQueue<NateVertex> priorityQueue = new PriorityQueue<NateVertex> ();
    // the priority queue that gives us the next node to process
    private Mesh mesh;
    private Dictionary<int, NateVertex> vertices = new Dictionary<int, NateVertex> ();

    public void StartAlgorithm () {
        // grab a reference to the GraphVisuals singleton
        //        GameObject singletons = GameObject.Find ("Singletons");
        //        graphVisuals = singletons.GetComponent<GraphVisuals> ();
        MeshFilter meshFilter = GetComponent<MeshFilter> ();
        mesh = meshFilter.mesh;
        int[] triangles = mesh.triangles;

        for (var i = 0; i < triangles.Length; i += 3) {
            var neighborIndices = new int[] { triangles [i], triangles [i + 1], triangles [i + 2] };
            SetVertices (neighborIndices);
            SetEdges (neighborIndices);
        }

        // TODO set start and end points
        StartNode = vertices [0];
        GoalNode = vertices [vertices.Count - 1];

        // kick-start the algorithm
        PerformAlgorithm = true;
    }

    private void SetVertices (int[] neighborIndices) {
        for (var i = 0; i < neighborIndices.Length; i++) {
            int currentVertex = neighborIndices [i];
            if (!vertices.ContainsKey (currentVertex)) {
                vertices [currentVertex] = new NateVertex (currentVertex, transform.TransformPoint (mesh.vertices [currentVertex]));
            }
        }
    }

    private void SetEdges (int[] neighborIndices) {
        NateVertex[] neighbors = neighborIndices.Select (j => vertices [j]).ToArray ();
        for (var i = 0; i < neighborIndices.Length; i++) {
            vertices [neighborIndices [i]].AssignEdges (neighbors);
        }
    }

    void Update () {
        if (StartNode == null || GoalNode == null) {
            // if either node is null then don't waste time processing unnecessary code
            return;
        }

        // only proceed if enough time has passed that another step should be performed
        if (Time.time >= timeOfNextStep) {
            /* If it is time for another step of the algorithm, go ahead and set
             * the expected time of the step after this one. I do this before
             * actually starting the work in case the processing takes a long
             * time. This makes for a better user experience.
             */
            timeOfNextStep += DelayBetweenSteps;
            if (!initialized) {
                // only initialize if necessary
                InitializeDijkstra ();
            }
            DoNextDijkstraStep (); // perform one step of the algorithm
        }
    }

    public bool PerformAlgorithm {
        /* I'm using the property PerformAlgorithm as a simple interface or proxy
             * for the more complicated value of timeOfNextStep. As long as timeOfNextStep
             * is infinity then the program should not run, because Time.time will never
             * be greater than or equal to infinity. So PerformAlgorithm is false when
             * timeOfNextStep is infinity, and PerformAlgorithm is true when timeOfNextStep
             * is not infinity. Similarly, we can pause the algorithm by setting PerformAlgorithm
             * to false, which sets timeOfNextStep to infinity; we can also unpause the
             * algorithm by setting PerformAlgorithm to true, which sets timeOfNextStep
             * to be equal to the sum of the current time (Time.time) plus the delay
             * before the next step (DelayBetweenSteps).
             *
             * This property has both a getter and a setter.
             */
        get { return (timeOfNextStep != float.PositiveInfinity); }
        set { timeOfNextStep = (value ? Time.time + DelayBetweenSteps : float.PositiveInfinity); }
    }

    public bool IsFinished {
        /* The algorithm is finished only if has already been initialized and the
             * priority queue is empty. If the priority queue is not empty then the
             * algorithm has started but has not yet finished. If the priority queue
             * is empty but the algorithm has not been initialized then the algorithm
             * has not yet been started and therefore could not be considered finished.
             *
             * This property has a getter but no setter. It is a read-only property.
             */
        get { return (initialized && priorityQueue.Count == 0); }
    }

    private void InitializeDijkstra () {
        if (StartNode == null || GoalNode == null) {
            System.Console.WriteLine ("ERROR: null nodes in InitializeDijkstra.");
            PerformAlgorithm = false;
            return;
        } else if (initialized) {
            System.Console.WriteLine ("ERROR: already initialized in InitializeDijkstra.");
            return;
        }

        // initialize start node and priority queue
        StartNode.LowestCostSoFar = 0;
        priorityQueue.Enqueue (StartNode);

        initialized = true;
    }

    private void DoNextDijkstraStep () {
        if (StartNode == null || GoalNode == null) {
            System.Console.WriteLine ("ERROR: null nodes in DoNextDijkstraStep.");
            PerformAlgorithm = false; // stop trying to run the algorithm
            return;
        } else if (IsFinished) {
            // if the algorithm has already finished then perhaps it is time to mark the shortest path
            if (!shortestPathMarked) {
                // only call the function to mark the shortest path if necessary
                MarkShortestPath ();
            } else {
                // if the algorithm has already finished and the shortest path has already been marked then there's really no reason DoNextDijkstraStep() should have been called
                System.Console.WriteLine ("ERROR: nothing to process in DoNextDijkstraStep.");
            }
            PerformAlgorithm = false; // stop trying to run the algorithm
            return;
        }

        // start of one step of Dijkstra's Algorithm
        NateVertex currentNode = priorityQueue.Dequeue ();
        if (currentNode != null) {
            foreach (NateEdge edge in currentNode.edges) {
                if (edge == null) {
                    continue;
                }

                MarkWithPrimitive (currentNode.position, Visited, Color.blue);

                NateVertex otherNode = edge.GetOtherVertex (currentNode);
                if (otherNode != null) {
                    // calculate cost using simple distance (for this assignment), also equal to (edge.transform.localScale.y * 2f)
                    float slope = (otherNode.position.y - currentNode.position.y) /
                                  (Mathf.Abs (otherNode.position.x - currentNode.position.x) + Mathf.Abs (otherNode.position.z - currentNode.position.z));
                    float verticalTraversalCost = otherNode.position.y * slope * VerticalTraversalCostMultiplier;
                    Vector3 weightedOtherPosition = new Vector3 (otherNode.position.x, otherNode.position.y + verticalTraversalCost, otherNode.position.z);
                    edge.Cost = (weightedOtherPosition - currentNode.position).magnitude;
                    float costOfPathPlusThisEdge = currentNode.LowestCostSoFar + edge.Cost;
                    if (costOfPathPlusThisEdge < otherNode.LowestCostSoFar) {
                        // update neighbor node if we have found a new shortest path to that node
                        otherNode.LowestCostSoFar = costOfPathPlusThisEdge;
                        otherNode.LowestCostEdgeSoFar = edge;
                        priorityQueue.Enqueue (otherNode);
                    }
                }
            }
        }
    }

    private void MarkShortestPath () {
        if (StartNode == null || GoalNode == null) {
            System.Console.WriteLine ("ERROR: null nodes in MarkShortestPath.");
            return;
        }

        // traverse the shortest path from the GoalNode backwards to the StartNode
        NateVertex node = GoalNode;
        NateEdge edge = node.LowestCostEdgeSoFar;
        while (edge != null) {
            MarkWithPrimitive (node.position, Path, Color.green);
            node = edge.GetOtherVertex (node);
            edge = node.LowestCostEdgeSoFar;
        }
        Destroy (Visited); 
        shortestPathMarked = true;
    }

    private void MarkWithPrimitive (Vector3 position, GameObject parent, Color color) {
        var cube = GameObject.CreatePrimitive (PrimitiveType.Cube);
        cube.transform.SetParent (parent.transform);
        cube.transform.position = position;
        cube.transform.localScale = new Vector3 (5f, 5f, 5f);
        var renderer = cube.GetComponent (typeof(Renderer)) as Renderer;
        renderer.material.color = color;
    }
}