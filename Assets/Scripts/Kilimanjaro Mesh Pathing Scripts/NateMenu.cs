﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System;

public class NateMenu : MonoBehaviour {
    public Slider mainSlider;
    public NateDijkstra controller;
    public Text costMultiplierText;

    public void Start () {
        SetNewValue (0f);
    }

    public void ValueChangeCheck () {
        SetNewValue (mainSlider.value * 10f);
    }

    public void StartAlgorithm () {
        controller.StartAlgorithm ();
        Destroy (gameObject);
    }

    private void SetNewValue (float val) {
        controller.VerticalTraversalCostMultiplier = val;
        costMultiplierText.text = val.ToString ();
    }
}
