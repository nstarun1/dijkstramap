﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NateVertex : IPrioritizable, IEnumerable {
    public int index;
    public Vector3 position;
    public HashSet<NateEdge> edges;

    public int Priority {
        get { return (int)LowestCostSoFar; }
    }

    public IEnumerator<NateEdge> GetEnumerator () {
        return edges.GetEnumerator ();
    }

    IEnumerator IEnumerable.GetEnumerator () {
        return edges.GetEnumerator ();
    }

    // initialize all nodes to have INFINITY cost and a NULL previous edge
    public float LowestCostSoFar = float.PositiveInfinity;
    public NateEdge LowestCostEdgeSoFar = null;

    public NateVertex (int vertexIndex, Vector3 vertexPosition) {
        var edgeEqualityComparer = new EdgeEqualityComparer ();
        edges = new HashSet<NateEdge> (edgeEqualityComparer);
        index = vertexIndex;
        position = vertexPosition;
    }

    public void AssignEdges (NateVertex[] neighboringVertices) {
        for (var i = 0; i < neighboringVertices.Length; i++) {
            AddEdge (neighboringVertices [i]);
        }
    }

    private void AddEdge (NateVertex neighboringVertex) {
        if (neighboringVertex.index == index) {
            return;
        }

        var edge = new NateEdge (this, neighboringVertex);
        edges.Add (edge);
    }
}

public class EdgeEqualityComparer : IEqualityComparer<NateEdge> {
    public bool Equals (NateEdge v1, NateEdge v2) {
        if (v2 == null && v1 == null)
            return true;
        else if (v1 == null | v2 == null)
            return false;
        else if ((v1.VertexA == v2.VertexA && v1.VertexB == v2.VertexB) ||
                 (v1.VertexB == v2.VertexA && v1.VertexA == v2.VertexB))
            return true;
        else
            return false;
    }

    public int GetHashCode (NateEdge edge) {
        int hCode = edge.VertexA.index ^ edge.VertexB.index;
        return hCode.GetHashCode ();
    }
}