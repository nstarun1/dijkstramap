﻿using UnityEngine;
using System.Collections;

public class NateEdge {
    public NateVertex VertexA;
    public NateVertex VertexB;
    public float Cost;

    public NateEdge (NateVertex vertexA, NateVertex vertexB) {
        VertexA = vertexA;
        VertexB = vertexB;
    }

    public NateVertex GetOtherVertex (NateVertex vertex) {
        NateVertex value = null;
        if (vertex == null) {
            System.Console.WriteLine ("ERROR: null vertex in GetOtherVertex.");
        } else if (vertex == VertexA) {
            value = VertexB;
        } else if (vertex == VertexB) {
            value = VertexA;
        } else {
            System.Console.WriteLine ("ERROR: vertex not connected to edge in GetOtherVertex.");
        }
        return value;
    }
}