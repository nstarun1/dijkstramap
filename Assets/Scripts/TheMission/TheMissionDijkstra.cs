﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TheMissionDijkstra : MonoBehaviour
{
	[SerializeField] private Toggle _mainStreets;
	[SerializeField] private Toggle _sideStreets;

	[SerializeField] private VertexScript _sampleMainStreetVertex;
	[SerializeField] private VertexScript _sampleSideStreetVertex;

	[SerializeField] private Text _titleText;
	[SerializeField] private Button _goButton;
	
	public float DelayBetweenSteps = 1f; // lets me slow down or speed up the process

	public VertexScript StartNode; // where Dijkstra's algorithm should start
	public VertexScript GoalNode; // the other end of the shortest path we are looking for

	private bool initialized = false; // flag for whether the algorithm has been initialized; notice that it is in the past tense
	private bool shortestPathMarked = false; // flag for whether the shortest path has been marked; notice that it is in the past tense
	private float timeOfNextStep = float.PositiveInfinity; // when the next step of the algorithm should occur; see PerformAlgorithm

	private IPriorityQueue<VertexScript> priorityQueue = new PriorityQueue<VertexScript> (); // the priority queue that gives us the next node to process

	private GraphVisuals graphVisuals; // a reference to the GraphVisuals singleton, provides us with materials for marking edges

	void Start()
	{
		// grab a reference to the GraphVisuals singleton
		GameObject singletons = GameObject.Find ("Singletons");
		graphVisuals = singletons.GetComponent<GraphVisuals> ();

		// kick-start the algorithm
		PerformAlgorithm = false;

		_goButton.interactable = false;
		_goButton.transform.Find("Text").GetComponent<Text>().text = "Waiting...";
		
		_titleText.text = "Click a Start Point";
	}

	public void SetStartVertex(VertexScript startVertex)
	{
		StartNode = startVertex;

		_titleText.text = "Click an End Point";
	}
		
	public void SetEndVertex(VertexScript endVertex)
	{
		if (GoalNode)
		{
			Renderer renderer = GoalNode.GetComponent<Renderer> ();
			renderer.material = graphVisuals.VertexMaterialsByHeight[0];
		}
		
		GoalNode = endVertex;

		_goButton.interactable = true;
		_goButton.transform.Find("Text").GetComponent<Text>().text = "Go!";
		
		_titleText.text = "Click Go!";
	}

	public void OnButtonClicked()
	{
		if(shortestPathMarked)
		{
			SceneManager.LoadScene("TheMission");
		}
		else if (!PerformAlgorithm)
		{
			PerformAlgorithm = true;
			_goButton.transform.Find("Text").GetComponent<Text>().text = "Running...";
			_goButton.interactable = false;

			_titleText.text = "Running...";
		}
	}

	void Update()
	{
		if (StartNode == null || GoalNode == null)
		{
			// if either node is null then don't waste time processing unnecessary code
			return;
		}

		// only proceed if enough time has passed that another step should be performed
		if (Time.time >= timeOfNextStep)
		{
			/* If it is time for another step of the algorithm, go ahead and set
			 * the expected time of the step after this one. I do this before
			 * actually starting the work in case the processing takes a long
			 * time. This makes for a better user experience.
			 */
			timeOfNextStep += DelayBetweenSteps;
			if (!initialized)
			{
				// only initialize if necessary
				InitializeDijkstra ();
			}
			DoNextDijkstraStep (); // perform one step of the algorithm
		}
	}

	public bool PerformAlgorithm
	{
		/* I'm using the property PerformAlgorithm as a simple interface or proxy
		 * for the more complicated value of timeOfNextStep. As long as timeOfNextStep
		 * is infinity then the program should not run, because Time.time will never
		 * be greater than or equal to infinity. So PerformAlgorithm is false when
		 * timeOfNextStep is infinity, and PerformAlgorithm is true when timeOfNextStep
		 * is not infinity. Similarly, we can pause the algorithm by setting PerformAlgorithm
		 * to false, which sets timeOfNextStep to infinity; we can also unpause the
		 * algorithm by setting PerformAlgorithm to true, which sets timeOfNextStep
		 * to be equal to the sum of the current time (Time.time) plus the delay
		 * before the next step (DelayBetweenSteps).
		 * 
		 * This property has both a getter and a setter.
		 */
		get { return (timeOfNextStep != float.PositiveInfinity); }
		set { timeOfNextStep = (value ? Time.time + DelayBetweenSteps : float.PositiveInfinity); }
	}

	public bool IsFinished
	{
		/* The algorithm is finished only if has already been initialized and the
		 * priority queue is empty. If the priority queue is not empty then the
		 * algorithm has started but has not yet finished. If the priority queue
		 * is empty but the algorithm has not been initialized then the algorithm
		 * has not yet been started and therefore could not be considered finished.
		 * 
		 * This property has a getter but no setter. It is a read-only property.
		 */
		get { return (initialized && priorityQueue.Count == 0); }
	}

	private void InitializeDijkstra ()
	{
		if (StartNode == null || GoalNode == null)
		{
			System.Console.WriteLine ("ERROR: null nodes in InitializeDijkstra.");
			PerformAlgorithm = false;
			return;
		}
		else if (initialized)
		{
			System.Console.WriteLine ("ERROR: already initialized in InitializeDijkstra.");
			return;
		}

		// mark all edges as unvisited
		GameObject edgeGroup = GameObject.Find ("EdgeGroup");
		Renderer[] edgeRenderers = edgeGroup.GetComponentsInChildren<Renderer>();
		foreach (Renderer renderer in edgeRenderers)
		{
			renderer.sharedMaterial = graphVisuals.EdgeUnvisitedMaterial;
		}

		// initialize start node and priority queue
		StartNode.LowestCostSoFar = 0;
		priorityQueue.Enqueue (StartNode);

		initialized = true;
	}

	private void DoNextDijkstraStep()
	{
		if (StartNode == null || GoalNode == null)
		{
			System.Console.WriteLine ("ERROR: null nodes in DoNextDijkstraStep.");
			PerformAlgorithm = false; // stop trying to run the algorithm
			return;
		}
		else if (IsFinished)
		{
			// if the algorithm has already finished then perhaps it is time to mark the shortest path
			if (!shortestPathMarked)
			{
				// only call the function to mark the shortest path if necessary
				MarkShortestPath ();
			}
			else
			{
				// if the algorithm has already finished and the shortest path has already been marked then there's really no reason DoNextDijkstraStep() should have been called
				System.Console.WriteLine ("ERROR: nothing to process in DoNextDijkstraStep.");
			}
			PerformAlgorithm = false; // stop trying to run the algorithm
			return;
		}

		// start of one step of Dijkstra's Algorithm
		VertexScript currentNode = priorityQueue.Dequeue ();
		if (currentNode != null)
		{
			foreach (EdgeScript edge in currentNode.Edges)
			{
				if (edge == null)
				{
					continue;
				}

				// mark edge as visited
				Renderer renderer = edge.GetComponent<Renderer> ();
				renderer.sharedMaterial = graphVisuals.EdgeVisitedMaterial;

				VertexScript otherNode = edge.GetOtherVertex (currentNode);
				if (otherNode != null)
				{
					float distance = (otherNode.transform.position - currentNode.transform.position).magnitude;
					
					edge.Cost = distance;
					
					if (_mainStreets.isOn && !_sideStreets.isOn)
					{
						if ((otherNode.transform.localScale == _sampleSideStreetVertex.transform.localScale))
						{
							edge.Cost = 10000;
						}
					}
					else if (!_mainStreets.isOn && _sideStreets.isOn)
					{
						if ((otherNode.transform.localScale == _sampleMainStreetVertex.transform.localScale))
						{
							edge.Cost = 10000;
						}
					}
					
					float costOfPathPlusThisEdge = currentNode.LowestCostSoFar + edge.Cost;
					if (costOfPathPlusThisEdge < otherNode.LowestCostSoFar)
					{
						// update neighbor node if we have found a new shortest path to that node
						otherNode.LowestCostSoFar = costOfPathPlusThisEdge;
						otherNode.LowestCostEdgeSoFar = edge;
						priorityQueue.Enqueue (otherNode);
						renderer.sharedMaterial = graphVisuals.EdgeInQueueMaterial; // mark edge as in queue
					}
				}
			}
		}
	}

	private void MarkShortestPath()
	{
		if (StartNode == null || GoalNode == null)
		{
			System.Console.WriteLine ("ERROR: null nodes in MarkShortestPath.");
			return;
		}

		// traverse the shortest path from the GoalNode backwards to the StartNode
		VertexScript node = GoalNode;
		EdgeScript edge = node.LowestCostEdgeSoFar;
		while (edge != null)
		{
			Renderer renderer = edge.GetComponent<Renderer> ();
			renderer.sharedMaterial = graphVisuals.ShortestPathMaterial;
			node = edge.GetOtherVertex (node);
			edge = node.LowestCostEdgeSoFar;
		}
		
		shortestPathMarked = true;

		_titleText.text = "Finished!";
		_goButton.interactable = true;
		_goButton.transform.Find("Text").GetComponent<Text>().text = "Reload";
	}
}