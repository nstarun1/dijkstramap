﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class VertexScript : MonoBehaviour, IPrioritizable
{
	public EdgeScript[] Edges = new EdgeScript[6];

	public int Priority
	{
		get { return (int)LowestCostSoFar; }
	}

	// initialize all nodes to have INFINITY cost and a NULL previous edge
	public float LowestCostSoFar = float.PositiveInfinity;
	public EdgeScript LowestCostEdgeSoFar = null;

	void Start()
	{
		// I color my vertices by height but that's not a requirement for Dijkstra's algorithm
		GameObject singletons = GameObject.Find ("Singletons");
		Renderer renderer = GetComponent<Renderer> ();
		renderer.sharedMaterial = singletons.GetComponent<GraphVisuals> ().GetVertexMaterialByHeight (transform.position.y);
	}

	private void OnMouseOver()
	{
		TheMissionDijkstra theMissionDijkstra = GameObject.FindObjectOfType<TheMissionDijkstra>();
		
		if (theMissionDijkstra && Input.GetMouseButtonUp(0) && !theMissionDijkstra.PerformAlgorithm && !theMissionDijkstra.IsFinished)
		{
			GameObject singletons = GameObject.Find ("Singletons");
			Renderer renderer = GetComponent<Renderer> ();
			
			if (!theMissionDijkstra.StartNode)
			{
				renderer.sharedMaterial = singletons.GetComponent<GraphVisuals>().VertexMaterialsByHeight[1];
				theMissionDijkstra.SetStartVertex(this);
			}
			else
			{
				renderer.sharedMaterial = singletons.GetComponent<GraphVisuals>().VertexMaterialsByHeight[2];
				theMissionDijkstra.SetEndVertex(this);
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		EdgeScript edge = other.gameObject.GetComponent<EdgeScript> ();
		if (edge)
		{
			for (int i = 0; i < Edges.Length; i++)
			{
				if (Edges [i] == edge)
				{
					break;
				}
				if (Edges [i] == null)
				{
					Edges [i] = edge;
					break;
				}
			}
		}
	}

	void OnTriggerExit(Collider other)
	{
		EdgeScript edge = other.gameObject.GetComponent<EdgeScript> ();
		if (edge)
		{
			for (int i = 0; i < Edges.Length; i++)
			{
				if (Edges [i] == edge)
				{
					for (int j = i + 1; j < Edges.Length; j++)
					{
						Edges [j - 1] = Edges [j];
					}
					Edges [Edges.Length - 1] = null;
					break;
				}
			}
		}
	}
}
